﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public static class Constants
    {
        public static string Usertoken { get; set; }
        public static string ProductsRepositoryURL { get; set; }
        public static string ShopperHistoryRepositoryURL { get; set; }
    }
}
