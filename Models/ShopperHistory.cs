﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
   public class ShopperHistory
    {
        public int customerId { get; set; }
        public List<Product> products { get; set; }
    }
}
