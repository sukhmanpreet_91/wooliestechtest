﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Cart
    {
        public List<Product> products { get; set; }
        public List<Special> specials { get; set; }
        public List<Product> quantities { get; set; }
    }
}
