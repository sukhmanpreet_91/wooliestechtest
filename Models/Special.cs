﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Special
    {
        public List<Product> quantities { get; set; }
        public int total { get; set; }
    }
}
