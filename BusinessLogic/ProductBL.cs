﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Models;
using System.Net.Http.Formatting;
using DAL;

namespace BusinessLogic
{
    public class Products
    {
        public List<Product> GetProducts(string sortOption)
        {
            ProductRepository productRepo = new ProductRepository();

            var products = productRepo.GetProducts();

            switch (sortOption.ToLower())
            {
                case "low":
                    products = products.OrderBy(x => x.price).ToList();
                    break;
                case "high":
                    products = products.OrderByDescending(x => x.price).ToList();
                    break;
                case "ascending":
                    products = products.OrderBy(x => x.name).ToList();
                    break;
                case "descending":
                    products = products.OrderByDescending(x => x.name).ToList();
                    break;
                case "recommended":
                    var shopHistory = GetShopperHistory();
                    var allProducts = shopHistory.SelectMany(x => x.products);
                    var popularProducts = allProducts.GroupBy(x => x.name).Select(y => new { name = y.Key,count= y.Sum(s => s.quantity)}).OrderByDescending(y=> y.count).Select(x => x.name).ToArray();

                    products = products.OrderBy(x => Array.IndexOf(popularProducts, x.name) >= 0? Array.IndexOf(popularProducts, x.name):int.MaxValue ).ToList();
                    
                    break;
                default:
                    break;
            }

            return products;
        }

        private List<ShopperHistory> GetShopperHistory() {
            ShopperHistoryRepository shopperHistoryRepository = new ShopperHistoryRepository();
            var shopHistory = shopperHistoryRepository.GetShopperHistories();
            return shopHistory;

        }

        public decimal GetTrolleyValue(Cart cart) {
            decimal trolleyValue = 0;
            foreach (var product in cart.quantities)
            {
                trolleyValue = trolleyValue + GetBestPrice(cart,product);
            }
            return trolleyValue;
        }

        private decimal GetBestPrice(Cart cart,Product product)
        {
            decimal price = 0;
            var specialPrice = cart.specials.Where(x => x.quantities.Any(y => y.name == product.name)).FirstOrDefault();
            if (specialPrice != null)
            {
                // Items in specials
                decimal specialPriceItemQuantity = specialPrice.quantities.Where(x => x.name == product.name).FirstOrDefault().quantity;


                var specialInclusionsPrice = (Math.Floor(product.quantity / specialPriceItemQuantity)) * specialPrice.total;
                price = price + specialInclusionsPrice;

                //remaining items if any, will be charged on full amount
                if (product.quantity % specialPriceItemQuantity > 0) price = price + (product.quantity % specialPriceItemQuantity) * cart.products.Where(x => x.name == product.name).FirstOrDefault().price;
            }
            return price;
        }


    }
}
