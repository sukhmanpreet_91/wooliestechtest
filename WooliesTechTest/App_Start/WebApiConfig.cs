﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace WooliesTechTest
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "UserApi",
                routeTemplate: "api/answers/user",
                defaults: new { controller = "Assessment", action = "GetUser", id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "ProductApi",
                routeTemplate: "api/products/sort",
                defaults: new { controller = "Assessment", action = "GetProducts", id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "TrolleyApi",
                routeTemplate: "api/products/trolleyCalculator",
                defaults: new { controller = "Assessment", action = "CalculateTrolleyValue", id = RouteParameter.Optional }
            );
            config.Routes.MapHttpRoute(
                name: "TrolleyCalApi",
                routeTemplate: "api/products/trolleyTotal",
                defaults: new { controller = "Assessment", action = "CalculateTrolleyValue", id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
