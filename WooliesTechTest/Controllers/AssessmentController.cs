﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WooliesTechTest.Models;
using BusinessLogic;
using System.Threading.Tasks;
using Models;

namespace WooliesTechTest.Controllers
{
    public class AssessmentController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            Products p = new Products();
            var res = p.GetProducts("test");
            return res.Select(x => x.name);
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        [HttpGet]
        [Route("api/answers/user")]
        public IHttpActionResult GetUser()
        {
            User user = new User();
            user.token = System.Configuration.ConfigurationManager.AppSettings["UserToken"].ToString();
            user.name = "Sukhmanpreet Singh";
            return Ok(user);
        }

        [HttpGet]
        [Route("api/products/sort")]
        public IHttpActionResult GetProducts([FromUri]string sortOption)
        {
            Products p = new Products();
            var res = p.GetProducts(sortOption);
            return Ok(res);
        }


        [HttpPost]
        public IHttpActionResult CalculateTrolleyValue([FromBody]Cart cart)
        {
            Products p = new Products();
            var res = p.GetTrolleyValue(cart);
            return Ok(res);
        }

    }
}
