﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Models;

namespace WooliesTechTest
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            InitializeConstants();
        }

        private void InitializeConstants()
        {
            Constants.Usertoken = System.Configuration.ConfigurationManager.AppSettings["UserToken"].ToString();
            Constants.ProductsRepositoryURL = System.Configuration.ConfigurationManager.AppSettings["ProductsBaseURL"].ToString();
            Constants.ShopperHistoryRepositoryURL = System.Configuration.ConfigurationManager.AppSettings["ShopperHistoryBaseURL"].ToString();
        }
    }
}
