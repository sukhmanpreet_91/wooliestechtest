﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WooliesTechTest.Models
{
    public class User
    {
        public string token { get; set; }
        public string name { get; set; }
    }
}