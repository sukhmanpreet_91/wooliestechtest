﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Models;

namespace DAL
{
    public class ShopperHistoryRepository
    {
        public List<ShopperHistory> GetShopperHistories()
        {
            HttpClient httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(Constants.ShopperHistoryRepositoryURL);
            List<ShopperHistory> shopperHistory = new List<ShopperHistory>();

            var response = httpClient.GetAsync(string.Format("?token={0}", Constants.Usertoken)).Result;

            if (response.IsSuccessStatusCode)
            {
                shopperHistory = response.Content.ReadAsAsync<List<ShopperHistory>>().Result;
            }
            return shopperHistory;
        }
    }
}
