﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Models;

namespace DAL
{
    public class ProductRepository
    {
        public List<Product> GetProducts()
        {
            HttpClient httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(Constants.ProductsRepositoryURL);
            List<Product> products = new List<Product>();

            var response = httpClient.GetAsync(string.Format("?token={0}", Constants.Usertoken)).Result;

            if (response.IsSuccessStatusCode)
            {
                products = response.Content.ReadAsAsync<List<Product>>().Result;
            }
            return products;

        }
    }
}
